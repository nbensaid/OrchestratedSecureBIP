#!/bin/bash

JAR_FILE=gen/BIPtoOrchBIP.jar
GEN_DIR=

# realpath not always installed...
getDirName() {
    if pushd $1 > /dev/null ; then
        pwd | tr '\n' '/'
        popd > /dev/null
    fi
}

error() {
    echo "error: $*" >&2
    exit 1
}

testInstall() {
    if test x"${BIPSEC_HOME}" != x"" -a -d "${BIPSEC_HOME}"; then
        BIPSEC_HOME=$(getDirName "${BIPSEC_HOME}")
        test -e "${BIPSEC_HOME}/${JAR_FILE}" && return 0
        return 1
    fi
    which -s java || return 1
    which -s jar || return 1
    return 1
}

run() {
    bip=$1
    annotation=$2
    test x"${bip}" != x"" -a -e "${bip}" || error "Bip file '${bip}' not found"

    BD=$(dirname $bip)
    BD=$(getDirName $BD)

    test -d "${BD}" || error "Source directory '${BD}' not found"

    if test x"${annotation}" == x"" -o ! -e "${annotation}"; then
        annotation=${BD}/IntercationAnnotation.xml
        test -e "${annotation}" || error "Annotation file '${annotation}' not found"
    fi

    if test x"${GEN_DIR}" != x""; then
        #test -d "${GEN_DIR}" || error "Could not find target directory ${GEN_DIR}"
        BD_DIR=$(basename ${BD})
        GEN_DIR="${GEN_DIR}/${BD_DIR}/"
    else
        GEN_DIR=${BD}/generated
    fi
    mkdir -p ${GEN_DIR} || error "Could not create directory ${GEN_DIR}"

    cp ${bip} ${annotation} ${GEN_DIR}/

    if pushd ${GEN_DIR} > /dev/null; then
        jar -xf ${BIPSEC_HOME}/${JAR_FILE} Protocol/Protocol.cpp
        mv Protocol/Protocol.cpp .
        rmdir Protocol

        java -jar "${BIPSEC_HOME}/${JAR_FILE}" $(basename ${bip}) $(basename ${annotation}) 0
        rm Protocol.cpp
        popd > /dev/null
    fi
}

testInstall || BIPSEC_HOME=$(dirname $0)

if ! testInstall; then
    echo "Java or Jar not found in ${BIPSEC_HOME} check installation"
    exit 1
fi

DEMO_DIR=./demo

if test $# -le 0; then
    echo "Usage: $0 [-o <gen_dir>] <bip file> [<annotation file>]"
    #run ./demo/BIP_examples/buybook20/BuyBook.bip
elif test $# -eq 1 -a "$1" == "demo" -a -d "${DEMO_DIR}"; then
    for bip in $(find ${DEMO_DIR} -iname '*.bip' ); do
        GEN_DIR=./generated/;
        run $bip
    done
else
    if test "$1" == "-o"; then
        GEN_DIR=$2;
        shift 2
    fi
    run $*
fi
