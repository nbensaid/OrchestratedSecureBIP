/*****************************
/* Model of EventCreation System 
/*****************************
/ The system consists of a finite number of Event-Receiver components communicating with
/ a set of  Event-Creator components. Initially, the Event-Creator componnet contacts, with multiparty
/ interactions, all Event-Receivers to create an event. The event can only be created if all
/ component are ready to gether for it. Once an event is created, the components can exchange between each other  messages.    
/ The security requirements  consist on:
/ 	- Some created events should be secret
/ 	- Some exchanged data on a public event should be also quept secret



/*****************************/
/* Definition of port types  */
/*****************************/
 model EventCreation
 port type dataport( int e)
 port type eport //port with no data

/*********************************/
/* Definition of connector types */
/*********************************/


connector type single_SendRec(dataport s, dataport r) // strong synchronization between s, r
  define s r 
  on s r 
    up {} 
    down {r.e=s.e;}
end

connector type Event_SendRec(eport s,  eport r, eport t) // strong event synchronization between s, r
  define s r t
  on s r t
end

/********************************************/
/* Definition of atomic type: Event-Creator */
/********************************************/
atomic type EventCreator
//@security(pl="Prosumer1:SMG")
  data string msg1  //Energy production value
  data string msg2  //Energy consumption value
  export port eport invite()
   port eport cancel()
  export port eport open()
  export port dataport store(msg1)
  export port dataport report(msg2)

  place l1 
  place l2
  place l3 
  place l4

  initial to l1 do {msg1="";msg2="";}
  on invite from l1 to l2 
  on cancel from l2 to l1
  on open from l2 to l3
  on store from l3 to l3 
  on report from l3 to l3 

end

/*********************************************/
/* Definition of atomic type: Event-Receiver */
/*********************************************/
atomic type EventReceiver
//@security(pl="Prosumer1:SMG")
  data string msg1  //Energy production value
  data string msg2  //Energy consumption value
  export port eport receive()
  export port eport enter()
  export port dataport push(msg1)
  export port dataport get(msg2)

  place l1 
  place l2
  place l3 
  place l4


  initial to l1 do {msg1="";msg2="";}
  on receive from l1 to l2 
  on enter from l2 to l3
  on push from l3 to l3 
  on get  from l3 to l3 

end


/********************************************/
/* definition of compound type: SmartGrid */
/********************************************/

compound type EventCreation
component EventCreator EC1
component EventReceiver ER1
component EventReceiver ER2
connector  Event_SendRec connection_invite1(EC1.invite, ER1.receive, ER2.receive)
connector  Event_SendRec connection_enter1(EC1.open, ER1.enter, ER2.enter)
connector single_SendRec data_exchange1(ER1.push, EC1.store)
connector single_SendRec data_exchange2(EC1.report, ER1.get)
connector single_SendRec data_exchange3(ER2.push, EC1.store)
connector single_SendRec data_exchange4(EC1.report, ER2.get)


component EventCreator EC2
component EventReceiver ER3
component EventReceiver ER4
connector  Event_SendRec connection_invite2(EC2.invite, ER3.receive, ER4.receive)
connector  Event_SendRec connection_enter2(EC2.open, ER3.enter, ER4.enter)
connector single_SendRec data_exchange5(ER3.push, EC2.store)
connector single_SendRec data_exchange6(EC2.report, ER3.get)
connector single_SendRec data_exchange7(ER4.push, EC2.store)
connector single_SendRec data_exchange8(EC2.report, ER4.get)


component EventCreator EC3
component EventReceiver ER5
component EventReceiver ER6
connector  Event_SendRec connection_invite3(EC3.invite, ER5.receive, ER6.receive)
connector  Event_SendRec connection_enter3(EC3.open, ER5.enter, ER6.enter)
connector single_SendRec data_exchange9(ER5.push, EC3.store)
connector single_SendRec data_exchange10(EC3.report, ER5.get)
connector single_SendRec data_exchange11(ER6.push, EC3.store)
connector single_SendRec data_exchange12(EC3.report, ER6.get)


component EventCreator EC4
component EventReceiver ER7
component EventReceiver ER8
connector  Event_SendRec connection_invite4(EC4.invite, ER7.receive, ER8.receive)
connector  Event_SendRec connection_enter4(EC4.open, ER7.enter, ER8.enter)
connector single_SendRec data_exchange13(ER7.push, EC4.store)
connector single_SendRec data_exchange14(EC4.report, ER7.get)
connector single_SendRec data_exchange15(ER8.push, EC4.store)
connector single_SendRec data_exchange16(EC4.report, ER8.get)


component EventCreator EC5
component EventReceiver ER9
component EventReceiver ER10
connector  Event_SendRec connection_invite5(EC5.invite, ER9.receive, ER10.receive)
connector  Event_SendRec connection_enter5(EC5.open, ER9.enter, ER10.enter)
connector single_SendRec data_exchange17(ER9.push, EC5.store)
connector single_SendRec data_exchange18(EC5.report, ER9.get)
connector single_SendRec data_exchange19(ER10.push, EC5.store)
connector single_SendRec data_exchange20(EC5.report, ER10.get)


component EventCreator EC6
component EventReceiver ER11
component EventReceiver ER12
connector  Event_SendRec connection_invite6(EC6.invite, ER11.receive, ER12.receive)
connector  Event_SendRec connection_enter6(EC6.open, ER11.enter, ER12.enter)
connector single_SendRec data_exchange21(ER11.push, EC6.store)
connector single_SendRec data_exchange22(EC6.report, ER11.get)
connector single_SendRec data_exchange23(ER12.push, EC6.store)
connector single_SendRec data_exchange24(EC6.report, ER12.get)


component EventCreator EC7
component EventReceiver ER13
component EventReceiver ER14
connector  Event_SendRec connection_invite7(EC7.invite, ER13.receive, ER14.receive)
connector  Event_SendRec connection_enter7(EC7.open, ER13.enter, ER14.enter)
connector single_SendRec data_exchange25(ER13.push, EC7.store)
connector single_SendRec data_exchange26(EC7.report, ER13.get)
connector single_SendRec data_exchange27(ER14.push, EC7.store)
connector single_SendRec data_exchange28(EC7.report, ER14.get)


component EventCreator EC8
component EventReceiver ER15
component EventReceiver ER16
connector  Event_SendRec connection_invite8(EC8.invite, ER15.receive, ER16.receive)
connector  Event_SendRec connection_enter8(EC8.open, ER15.enter, ER16.enter)
connector single_SendRec data_exchange29(ER15.push, EC8.store)
connector single_SendRec data_exchange30(EC8.report, ER15.get)
connector single_SendRec data_exchange31(ER16.push, EC8.store)
connector single_SendRec data_exchange32(EC8.report, ER16.get)


component EventCreator EC9
component EventReceiver ER17
component EventReceiver ER18
connector  Event_SendRec connection_invite9(EC9.invite, ER17.receive, ER18.receive)
connector  Event_SendRec connection_enter9(EC9.open, ER17.enter, ER18.enter)
connector single_SendRec data_exchange33(ER17.push, EC9.store)
connector single_SendRec data_exchange34(EC9.report, ER17.get)
connector single_SendRec data_exchange35(ER18.push, EC9.store)
connector single_SendRec data_exchange36(EC9.report, ER18.get)



end
component EventCreation ECreation
end





