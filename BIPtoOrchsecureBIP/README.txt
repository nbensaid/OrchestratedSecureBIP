*************************************
   BIPtoOrchBIP Transformation
   NAJAH BEN SAID
   Version1.0 TAMIS
*************************************

The tool takes as input a centralized secureBIP file with multiparty interactions and generates a decentralized secureBIP model with orchestrator components.
The synchronized multi-party interactions are transformed into Send/Receive message passing.


A- Package details
------------------

 The project contains the following files and directories:
	- gen : contains the runnable jar for the system execution
	- demo: contains the examples used to validate the tool
	- BIPtoOrchBIP: script to lunch the tool


B- Prerequisites
----------------

    - Linux Ubuntu 16.04 distribution

    - Java JRE 1.6 (or higher)

C- Runing steps
---------------
The tool can be operated using the provided script "BIPtoOrchBIP.sh":
    Usage: ./BIPtoOrchBIP.sh [-o <gen_dir>] <bip file> [<annotation file>]
Options:
        <bip file>:         the input bip file. Its directory will be used to find the annotation file eventually.
        -o <gen dir>        optional, used to specify the output directory.
                            By default, the output directory "generated" is created in the directory of the bip file.
        <annotation file>   optional, the annotation file. By default the file "IntercationAnnotation.xml" is taken in
                            the directory of the bip file.

example:
        ./BIPtoOrchBIP.sh demo/BIP_examples/buybook1000/BuyBook.bip 

To run the tool directly you have to type in a terminal the following command:
       java -jar ./gen/BIPtoOrchBIP.jar <input_bip_file> <Annotation_file> <partition=0>
example:
       java -jar ./gen/BIPtoOrchBIP.jar ./demo/BuyBook.bip  ./demo/IntercationAnnotation.xml 0

