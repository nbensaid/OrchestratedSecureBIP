package owsm.custom.soa;

import java.io.UnsupportedEncodingException;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import oracle.security.crypto.util.Utils;

/**
 * A singleton class for encrypting and decrypting. Implemented as a singleton
 * so that the creation of the ciphers only need be done once instead of for
 * each message. (This could probably be done with static variables as well.)
 * 
 * This class assumes that only one key will ever be used. If you will use
 * multiple keys then this class needs to be modified to track multiple keys.
 * 
 * This class can be called by any other classes that want to encrypt or decrypt,
 * including being called from a BPEL process.
 */
public class EncDec
{
  //////////////////////////////////////////////////////////////////////////////
  //
  // Types
  //
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //
  // Inner Classes
  //
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //
  // Constants
  //
  //////////////////////////////////////////////////////////////////////////////

  private static final String UTF8 = "UTF-8";

  //////////////////////////////////////////////////////////////////////////////
  //
  // Member Variables
  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * A reference for the singleton.
   */
  private static EncDec mEncDec = null;
  
  /**
   * Cipher for encryption.
   */
  private Cipher mECipher;
  
  /**
   * Cipher for decryption.
   */
  private Cipher mDCipher;
  
  //////////////////////////////////////////////////////////////////////////////
  //
  // Constructors
  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Perform one-time initialisation needed whenever this class is instantiated.
   * A private constructor in order to defeat instantiation.
   */
  private EncDec()
  {
    // Nothing to do.
  }

  //////////////////////////////////////////////////////////////////////////////
  //
  // Getters and Setters
  //
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //
  // Interface Methods
  //
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //
  // Abstract Methods
  //
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //
  // Overridden Parent Methods
  //
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //
  // Public Static Methods
  //
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //
  // Protected Static Methods
  //
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //
  // Private Static Methods
  //
  //////////////////////////////////////////////////////////////////////////////

  private static SecretKey deriveKey(char[] password)
  {
    try
    {
      MessageDigest md = MessageDigest.getInstance("SHA1");
      byte digest[] = md.digest(new String(password).getBytes("UTF-8"));
      //return first 128 bytes to get AES 128 byte key
      return new SecretKeySpec(digest, 0, 16, "AES");
    }
    catch (NoSuchAlgorithmException ex)
    {
      // ignore, SHA1 is always present
    }
    catch (UnsupportedEncodingException ex)
    {
      // ignore, UTF-8 is always present
    }
    return null;
  } // deriveKey()

  //////////////////////////////////////////////////////////////////////////////
  //
  // Public Methods
  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Get an instance of this class
   * 
   * @return a singleton instance of this class
   */
  public static synchronized EncDec getInstance()
  {
    if (mEncDec == null)
    {
      mEncDec = new EncDec();
    }
    return mEncDec;
  } // getInstance()

  //////////////////////////////////////////////////////////////////////////////

  /**
   * Encrypt the given string. The key is only required the first time as the
   * encryption cipher is permanent once created
   * 
   * @param strToEncrypt the string to encrypt
   * @param key the key to base the cipher on. The key only needs to be supplied
   *            for the initial call. After that you can pass in null
   * @return the encrypted string
   */
  public String encryptStrToStr(String strToEncrypt, String key)
  {
    try
    {
      // Generate a new random 16 byte IV
      byte iv[] = new byte[16];
      SecureRandom scr = new SecureRandom();
      scr.nextBytes(iv);

      // If this is the first time, the create the cipher based on the key.
      //
      if (mECipher == null)
      {
        mECipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        mECipher.init(Cipher.ENCRYPT_MODE, deriveKey(key.toCharArray()), new IvParameterSpec(iv));
      }

      byte encBytes[] = mECipher.doFinal(strToEncrypt.getBytes(UTF8));

      // prepend the iv to the encBytes;
      byte encBytes2[] = new byte[iv.length + encBytes.length];
      System.arraycopy(iv, 0, encBytes2, 0, iv.length);
      System.arraycopy(encBytes, 0, encBytes2, iv.length, encBytes.length);

      // Base64 encode it
      return Utils.toBase64(encBytes2);
      
    }
    catch (NoSuchAlgorithmException ex)
    {
      // ignore, SHA1 is always present
    }
    catch (NoSuchPaddingException ex)
    {
      // ignore, PKCS5Padding is always present
    }
    catch (InvalidAlgorithmParameterException ex)
    {
      // ignore, IvParameterSpec is always allowed in CBC mode
    }
    catch (InvalidKeyException ex)
    {
      // ignore, AES key will always work with AES Cipher
    }
    catch (BadPaddingException ex)
    {
      // ignore, Bad Padding doesn't happen during encrypt
    }
    catch (IllegalBlockSizeException ex)
    {
      // ignore, illegal block size doesn't happen during encrypt
    }
    catch (UnsupportedEncodingException ex)
    {
      // ignore, UTF-8 is always present
    }
    return strToEncrypt;
  } // encryptStrToStr()

  //////////////////////////////////////////////////////////////////////////////

  /**
   * Decrypt the given string. The key is only required the first time as the
   * decryption cipher is permanent once created
   * 
   * @param strToDecrypt the string to decrypt
   * @param key the key to base the cipher on. The key only needs to be supplied
   *            for the initial call. After that you can pass in null
   * @return the decrypted string
   */
  public String decryptStrToStr(String strToDecrypt, String key)
  {
    try
    {
      // Do a Base64 decode
      byte encBytes[] = Utils.fromBase64(strToDecrypt);
      
      // If this is the first time, the create the cipher based on the key.
      //
      if (mDCipher == null)
      {
        // The first 16 bytes of this is IV, 
        // the remaining bytes are the encrypted Data
        mDCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        mDCipher.init(Cipher.DECRYPT_MODE, deriveKey(key.toCharArray()), new IvParameterSpec(encBytes, 0, 16));         
      }
      
      byte clearBytes[] = mDCipher.doFinal(encBytes, 16, encBytes.length-16);

      // Convert to a String
      return new String(clearBytes, UTF8);
      
    }
    catch (NoSuchAlgorithmException ex)
    {
      // ignore, SHA1 is always present
    }
    catch (NoSuchPaddingException ex)
    {
      // ignore, PKCS5Padding is always present
    }
    catch (InvalidAlgorithmParameterException ex)
    {
      // ignore, IvParameterSpec is always allowed in CBC mode
    }
    catch (InvalidKeyException ex)
    {
      // ignore, AES key will always work with AES Cipher
    }
    catch (BadPaddingException ex)
    {
      // ignore, Bad Padding doesn't happen during encrypt
    }
    catch (IllegalBlockSizeException ex)
    {
      // ignore, illegal block size doesn't happen during encrypt
    }
    catch (UnsupportedEncodingException ex)
    {
      // ignore, UTF-8 is always present
    }
    return strToDecrypt;
  } // decryptStrToStr()

  //////////////////////////////////////////////////////////////////////////////
  //
  // Protected Methods
  //
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //
  // Private Methods
  //
  //////////////////////////////////////////////////////////////////////////////
  
} // EncDec
