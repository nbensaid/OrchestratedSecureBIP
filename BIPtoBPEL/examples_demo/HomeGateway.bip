/**********************************
/* Model of Home Gateway System 
/**********************************
/The Home Gateway system collects information about the temperature and the presence of people in a building, 
/then analyses them and takes decisions to decrease or increase the temperature value according to the presence value.
/The associated security property is that the presence of people in the building is considered secret. Thus, an intruder
/ observing the public temperature variation should not be able to deduce the presence value.
/The platform consists of two machines directly connected by a cable.
/The presence detector and the component in charge of analysis are both deployed on the same machine.



/*****************************/
/* Definition of port types  */
/*****************************/
package HomeGateway
 port type dataport( int x) //port with associated data
 port type eport() //port with  no associated data

/*********************************/
/* Definition of connector types */
/*********************************/
connector type SendRec(dataport s, dataport r) // strong synchronization between s, r
  define s r 
  on s r 
    up {} 
    down {r.x=s.x;}
end

/*****************************************/
/* Definition of atomic type: TempDevice */
/*****************************************/
atom type TempDevice()
  data int deltaT  //temperature value to rectify
@security(pl="TempDevice: Analyser;PresSensor: Analyser")
  data int  T  //collected temperature value
  export port dataport outT(T)
  export port dataport inT(deltaT)
  port eport rectify()
  port eport internal1()


  place idle, send, recv, update

  initial to idle do{deltaT=0;T=17;}
  on internal1 from idle to send do {T=rand(T);}
  on outT from send to recv 
  on inT from recv to update 
  on rectify from update to idle do {T=f(deltaT);}
end


/*****************************************/
/* Definition of atomic type: PresSensor */
/*****************************************/
atom type PresSensor()
@security(pl="PresSensor: Analyser")
  data int P  //Presence value 
  port eport check()
  export port dataport outP(P)  

  place idle, send

  initial to idle
  on check from idle to send  do {P=rand(P);}
  on outP from send to idle
end



/***************************************/
/* Definition of atomic type: Analysis */
/***************************************/
atom type Analysis()
  data int teta1,teta2  
  data int T, P, deltaT
  export port dataport inT(T)
  export port dataport inP(P)
  export port dataport out(deltaT)
  port eport internal1(),internal2()

  place idle, recvT, recvP, compare, exit

  initial to idle do{teta1=15; teta2=27;}
  on inT from idle to recvT
  on inP from recvT to recvP
  on internal1 from recvP to compare provided((P==1) && (T!=teta1)) do {deltaT= teta1-T;}
  on internal2 from recvP to compare provided((P==0) && (T!=teta2)) do {deltaT= teta2-T;} 
  on out from compare to exit 
end

/********************************************/
/* definition of compound type: HomeGateway */
/********************************************/

compound type HomeGateway()
  component TempDevice T()
  component PresSensor P()
  component Analysis A()

  connector SendRec out_in_T(T.outT, A.inT)
  connector SendRec out_in_P(P.outP, A.inP)
  connector SendRec out_in_deltaT(A.out, T.inT)
end

end





