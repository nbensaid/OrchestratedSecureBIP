*************************************
   BIPtoBPEL Translator
   NAJAH BEN SAID
   Version1.0 TAMIS
*************************************


This project contain a tool that translates the orchestrator components in a decentralized bip file and generates orchestrator web services processes.


A- Package details
------------------
This folder contains:
    - examples_demo directory contains some demo systems
    - gen directory  contains the tooljar file to launch
    - default.bpel file is the generated bpel file that contains the orchestrator process
    - templates directory contains a template file used by the tool
    - CustomEncryptionJava: used encription/Decryption classes
    - README file that describes the tool.


B- Prerequisites
----------------

- Linux Ubuntu 16.04 distribution

- Java JRE 1.6 (or higher)




C- Runing steps
---------------

1. Dowload, extract and setup BIP environment (you will need it to build BIP models).

2. Run the command : java -jar ./gen/BIPtoBPEl.jar  -I <path_to_bip_file> -p  <bip_file_name> -d "<bip_package>"



D- Example
----------

1. Create a decentralized bip that contains orchestrator components like the one in 'examples_demo'

2. run in a terminal the command:    java -jar ./gen/BIPtoBPEl.jar -I <path_to_bip_file>  -p  <bip_file_name> -d "<bip_package>"
example: java -jar ./gen/BIPtoBPEl.jar  -I examples_demo -p  ProducerConsumer -d "ProducerConsumer()"  to launch the ProducerConsumer.bip example




