#!/bin/bash


JAR_FILE=gen/BPEL2BIP.jar
GEN_DIR=

# realpath not always installed...
getDirName() {
    if pushd $1 > /dev/null ; then
        pwd | tr '\n' '/'
        popd > /dev/null
    fi
}

error() {
    echo "error: $*" >&2
    exit 1
}

testInstall() {
    if test x"${BPEL2BIP_HOME}" != x"" -a -d "${BPEL2BIP_HOME}"; then
        BPEL2BIP_HOME=$(getDirName "${BPEL2BIP_HOME}")
        test -e "${BPEL2BIP_HOME}/${JAR_FILE}" && return 0
        return 1
    fi
    which -s java || return 1
    which -s jar || return 1
    return 1
}

run() {
    bpel="$1"
    test x"${bpel}" != x"" -a -e "${bpel}" || error "Bpel file '${bpel}' not found"

    BD="$(dirname "$bpel")"
    BD="$(getDirName $BD)"
    BN="$(basename "${bpel}" .bpel)"

    test -d "${BD}" || error "Source directory '${BD}' not found"
    test "${BN}" != "$(basename ${bpel})" || error "Input file not a bpel file"

    if test x"${GEN_DIR}" != x""; then
        #test -d "${GEN_DIR}" || error "Could not find target directory ${GEN_DIR}"
        BD_DIR="$(basename ${BD})"
        GEN_DIR="${GEN_DIR}/${BD_DIR}/"
    else
        GEN_DIR="${BD}/generated"
    fi
    mkdir -p "${GEN_DIR}" || error "Could not create directory ${GEN_DIR}"

    cp "${bpel}" "${GEN_DIR}/"

    if pushd "${GEN_DIR}" > /dev/null; then
        echo "------------------------"
        echo "in ${GEN_DIR} run '${BN}.bpel' '${BN}.bip'"
        java -jar "${BPEL2BIP_HOME}/${JAR_FILE}" "${BN}.bpel" "${BN}.bip" || error "execution failure"
        popd > /dev/null
    fi
}

testInstall || BPEL2BIP_HOME="$(dirname $0)"

if ! testInstall; then
    echo "Java or Jar not found in ${BPEL2BIP_HOME} check installation"
    exit 1
fi

DEMO_DIR=./demo

if test $# -le 0; then
    echo "Usage: $0 [-o <gen_dir>] <bpel file> [<bip file>]"
    #run ./demo/BIP_examples/buybook20/BuyBook.bip
elif test $# -eq 1 -a "$1" == "demo" -a -d "${DEMO_DIR}"; then
    for bpel in $(find ${DEMO_DIR} -iname '*.bpel' | tr ' ' % ); do
        GEN_DIR=./generated/;
        run "$(echo $bpel | tr % ' ' )"
    done
else
    if test "$1" == "-o"; then
        GEN_DIR=$2;
        shift 2
    fi
    run $*
fi
